'use strict'

alert('Калькулятор');
let firstNumber = prompt("Введите первое число");
console.log(+firstNumber);
console.log(typeof +firstNumber);

if (firstNumber === '') {
    alert("Первое число не указано");
}
if (isNaN(+firstNumber)) {
    alert("Некорректный ввод чисел");
}

let secondNumber = prompt("Введите второе число");
console.log(+secondNumber);
console.log(typeof +secondNumber);

if (secondNumber === '') {
    alert("Второе число не указано");
}
if (isNaN(+secondNumber)) {
    alert("Некорректный ввод чисел");
}

let operand = prompt("Введите операцию");
console.log(operand);

if (operand === "") {
    console.log(operand);
    alert("Не введен знак");

}

if (operand === "+") {
    console.log(+firstNumber + +secondNumber);
    alert(+firstNumber + +secondNumber);
}

if (operand === "-") {
    console.log(+firstNumber - +secondNumber);
    alert(+firstNumber - +secondNumber);
}

if (operand === "*") {
    console.log(+firstNumber * +secondNumber);
    alert(+firstNumber * +secondNumber);
}

if (operand === "/") {
    console.log(+firstNumber - +secondNumber);
    alert(+firstNumber / +secondNumber);
}

else if (operand !== "+" && operand !== "-" && operand !== "*" && operand !== "/") {
    console.log(operand);
    alert("Программа не поддерживает такую операцию");
}

if (isNaN(+firstNumber + +secondNumber) && isNaN(+firstNumber - +secondNumber) && isNaN(+firstNumber * +secondNumber) && isNaN(+firstNumber / +secondNumber)) {
    alert("Некорректная операция!");
}
if ((firstNumber === '') && (secondNumber === '')) {
    alert("Некорректная операция!");
}
if ((secondNumber === '0') && (operand === "/")) {
    alert("На ноль делить нельзя!");
}